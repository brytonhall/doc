# doc [![GoDoc](http://godoc.org/gitlab.com/brytonhall/doc/markdown?status.svg)](http://godoc.org/gitlab.com/brytonhall/doc/markdown) [![License](https://img.shields.io/badge/licence-BSD-red.svg)](https://opensource.org/licenses/BSD-2-Clause)

A modern lightweight markup language.


## install

    go get gitlab.com/brytonhall/doc


## standards

`doc` is currently refining its own standard.


## usage

There is a [library](markdown/markdown.go):

``` go
md := markdown.New(markdown.XHTMLOutput(true), markdown.Nofollow(true))
fmt.Println(md.RenderToString([]byte("Header\n===\nText")))
```

and a command line [tool](main.go).

The following options are supported:

  Name            |  Type  |                        Description                          | Default
  --------------- | ------ | ----------------------------------------------------------- | ---------
  HTML            | bool   | whether to enable raw HTML                                  | false
  Tables          | bool   | whether to enable GFM tables                                | true
  Linkify         | bool   | whether to autoconvert plain-text URLs to links             | true
  Typographer     | bool   | whether to enable typographic replacements                  | true
  Quotes          | string | double + single quote replacement pairs for the typographer | “”‘’
  MaxNesting      | int    | maximum nesting level                                       | 20
  LangPrefix      | string | CSS language prefix for fenced blocks                       | language-
  Breaks          | bool   | whether to convert newlines inside paragraphs into `<br>`   | false
  Nofollow        | bool   | whether to add `rel="nofollow"` to links                    | false
  XHTMLOutput     | bool   | whether to output XHTML instead of HTML                     | false


## TODO

  * Improve performance with the raw HTML option enabled
  * Write an URL parser/encoder that would support decoding punycode and counting matching `[(` and `)]` in URLs


## references

`doc` was forked from [golang-commonmark/markdown](https://github.com/golang-commonmark/markdown).
