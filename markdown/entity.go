package markdown

import "github.com/golang-commonmark/markdown/html"

func ruleEntity(s *StateInline, silent bool) (_ bool) {
	pos := s.Pos
	src := s.Src

	if src[pos] != '&' {
		return
	}

	max := s.PosMax

	if pos+1 < max {
		if e, n := html.ParseEntity(src[pos:]); n > 0 {
			if !silent {
				s.Pending.WriteString(e)
			}
			s.Pos += n
			return true
		}
	}

	if !silent {
		s.Pending.WriteByte('&')
	}
	s.Pos++

	return true
}
